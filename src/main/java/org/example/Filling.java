package org.example;

public class Filling {
    private String type;
    private int cost;

    public Filling(String type) {
        this.type = type;
        if(type.equals("milk")){
            this.cost = 1000;
        }else if(type.equals("chocolate")){
            this.cost = 1200;
        }
    }

    public int getCost() {
        return cost;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
