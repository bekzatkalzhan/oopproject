package org.example;

public class Decorator {
    private String type;
    private int cost;

    public Decorator(String type) {
        this.type = type;
        if(type.equals("oreo")){
            this.cost = 300;
        }else if(type.equals("bablgam")){
            this.cost = 250;
        }
    }

    public int getCost() {
        return cost;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
