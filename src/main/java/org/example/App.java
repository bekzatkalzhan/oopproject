package org.example;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Scanner in = new Scanner(System.in);
        System.out.println( "Your name: " );
        String name = in.nextLine();
        Screen screen = new Screen(name);
        screen.order();
    }
}
