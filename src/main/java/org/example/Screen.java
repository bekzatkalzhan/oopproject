package org.example;

import java.util.*;

public class Screen {

    private final Map<Integer, String> FILLINGS = new HashMap<Integer, String>(){{
        put(1, "milk");
        put(2, "chocolate");
    }};

    private final Map<Integer, String> DECORATIONS = new HashMap<Integer, String>(){{
        put(1, "oreo");
        put(2, "bablgam");
    }};

    private String consumer;

    public Screen(String consumer) {
        this.consumer = consumer;
    }

    public IceCream order(){
        System.out.println(welcomeComsumer(this.consumer));
        System.out.println("What filling do you want?");
        String fillingName = chooseFilling();
        Filling filling = new Filling(fillingName);
        System.out.println("What decorations to add?");
        String[] decorationsNames = chooseDecorations();
        ArrayList<Decorator> tempDecorators = new ArrayList<>();
        for(String decorationName : decorationsNames){
            Decorator decorator = new Decorator(decorationName);
            tempDecorators.add(decorator);
        }
        Decorator[] decorators = new Decorator[tempDecorators.size()];
        decorators = tempDecorators.toArray(decorators);
        IceCream iceCream = new IceCream(filling, decorators);
        boolean isOk = payment(iceCream);
        if(isOk){
            return iceCream;
        }
        System.out.println("Not enough, try another time!");
        return null;
    }

    private String welcomeComsumer(String consumerName){
        return "Welcome to ICE CREAM SHOP, " + consumerName;
    }

    private String chooseFilling(){
        for (Map.Entry<Integer, String> set :
                FILLINGS.entrySet()) {
            System.out.println(set.getKey() + ". "
                    + set.getValue());
        }
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number :");
        int num = in.nextInt();
        in.close();
        return FILLINGS.get(num);
    }

    private String[] chooseDecorations(){
        ArrayList<String> decorators = new ArrayList<>();
        displayChoosingDec();
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number :");
        int num = in.nextInt();
        decorators.add(DECORATIONS.get(num));
        in.close();
        while(num != DECORATIONS.size()){
            System.out.println("Anything else?");
            displayChoosingDec();
            Scanner in2 = new Scanner(System.in);
            int num2 = in2.nextInt();
            if(num2 == DECORATIONS.size()){
                in2.close();
                break;
            }
            decorators.add(DECORATIONS.get(num2));
            in2.close();
        }
        String[] arr = new String[decorators.size()];
        arr = decorators.toArray(arr);
        return arr;
    }

    private void displayChoosingDec(){
        for (Map.Entry<Integer, String> set :
                DECORATIONS.entrySet()) {
            System.out.println(set.getKey() + ". "
                    + set.getValue());
        }
        System.out.println((DECORATIONS.size() + 1) + ". nothing");
    }

    private boolean payment(IceCream iceCream){
        System.out.println("Cost of the ice cream is: " + iceCream.getCost());
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the money :");
        int num = in.nextInt();
        if(num > iceCream.getCost()){
            System.out.println("Your change: " + (num - iceCream.getCost()));
            System.out.println("Thank you for purchase!");
            return true;
        } else if(num == iceCream.getCost()){
            System.out.println("Thank you for purchase!");
            return true;
        }
        return false;
    }
}
