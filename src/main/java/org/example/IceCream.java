package org.example;

import java.util.Arrays;

public class IceCream {
    private Filling filling;
    private Decorator[] decorators;
    private int cost;

    public IceCream(Filling filling, Decorator[] decorators) {
        this.filling = filling;
        this.decorators = decorators;
    }

    public Filling getFilling() {
        return filling;
    }

    public Decorator[] getDecorators() {
        return decorators;
    }

    public int getCost(){
        int total = this.filling.getCost();
        for (Decorator decorator : this.decorators){
            total += decorator.getCost();
        }
        return total;
    }
}
